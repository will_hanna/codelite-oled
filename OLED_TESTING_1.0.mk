##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=OLED_TESTING_1.0
ConfigurationName      :=Debug
WorkspacePath          :=/home/william/projects/oled_testing/OLED_TESTING_1.0
ProjectPath            :=/home/william/projects/oled_testing/OLED_TESTING_1.0
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=William
Date                   :=14/12/19
CodeLitePath           :=/home/william/.codelite
LinkerName             :="/opt/arm-none-eabi/bin/arm-none-eabi-gcc"
SharedObjectLinkerName :="/opt/arm-none-eabi/bin/arm-none-eabi-gcc" -dynamiclib -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)__SAMDA1J16A__ $(PreprocessorSwitch)DEBUG $(PreprocessorSwitch)TC_ASYNC=true $(PreprocessorSwitch)BOARD=SAMDA1_XPLAINED_PRO $(PreprocessorSwitch)__SAMDA1J16A__ $(PreprocessorSwitch)ARM_MATH_CM0PLUS=true 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="OLED_TESTING_1.0.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -mthumb -mabi=aapcs -mcpu=cortex-m0 -T samda1j16a_flash.ld -Wl,--gc-sections --specs=nano.specs -lc -lnosys
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)src $(IncludeSwitch)src/ASF $(IncludeSwitch)src/ASF/common $(IncludeSwitch)src/ASF/common/boards/ $(IncludeSwitch)src/ASF/common/utils/ $(IncludeSwitch)src/ASF/common/utils/interrupt/ $(IncludeSwitch)src/ASF/sam0/ $(IncludeSwitch)src/ASF/sam0/boards/ $(IncludeSwitch)src/ASF/sam0/boards/samda1_xplained_pro/ $(IncludeSwitch)src/ASF/sam0/drivers/ $(IncludeSwitch)src/ASF/sam0/drivers/port/ $(IncludeSwitch)src/ASF/sam0/drivers/port/quick_start/ $(IncludeSwitch)src/ASF/sam0/drivers/sercom/ $(IncludeSwitch)src/ASF/sam0/drivers/sercom/i2c/ $(IncludeSwitch)src/ASF/sam0/drivers/system/ $(IncludeSwitch)src/ASF/sam0/drivers/system/clock/ $(IncludeSwitch)src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/ $(IncludeSwitch)src/ASF/sam0/drivers/system/interrupt/ $(IncludeSwitch)src/ASF/sam0/drivers/system/interrupt/system_interrupt_samda/ $(IncludeSwitch)src/ASF/sam0/drivers/system/pinmux/ $(IncludeSwitch)src/ASF/sam0/drivers/system/pinmux/quick_start/ $(IncludeSwitch)src/ASF/sam0/drivers/system/power/ $(IncludeSwitch)src/ASF/sam0/drivers/system/power/power_sam_d_r/ $(IncludeSwitch)src/ASF/sam0/drivers/system/reset/ $(IncludeSwitch)src/ASF/sam0/drivers/system/reset/reset_sam_d_r/ $(IncludeSwitch)src/ASF/sam0/drivers/tc/ $(IncludeSwitch)src/ASF/sam0/drivers/tc/tc_sam_d_r/ $(IncludeSwitch)src/ASF/sam0/utils/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/include/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/include/component/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/include/instance/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/include/pio/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/source/ $(IncludeSwitch)src/ASF/sam0/utils/cmsis/samda1/source/gcc/ $(IncludeSwitch)src/ASF/sam0/utils/header_files/ $(IncludeSwitch)src/ASF/sam0/utils/linker_scripts/ $(IncludeSwitch)src/ASF/sam0/utils/linker_scripts/samda1/ $(IncludeSwitch)src/ASF/sam0/utils/linker_scripts/samda1/gcc/ $(IncludeSwitch)src/ASF/sam0/utils/make/ $(IncludeSwitch)src/ASF/sam0/utils/preprocessor/ $(IncludeSwitch)src/ASF/sam0/utils/syscalls/ $(IncludeSwitch)src/ASF/sam0/utils/syscalls/gcc/ $(IncludeSwitch)src/ASF/thirdparty/ $(IncludeSwitch)src/ASF/thirdparty/CMSIS/ $(IncludeSwitch)src/ASF/thirdparty/CMSIS/Include/ $(IncludeSwitch)src/ASF/thirdparty/CMSIS/Lib/ $(IncludeSwitch)src/ASF/thirdparty/CMSIS/Lib/GCC/ $(IncludeSwitch)src/config/ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)src/ASF/sam0/utils/linker_scripts/samda1/gcc/ $(LibraryPathSwitch)/opt/arm-none-eabi/arm-none-eabi/lib/thumb $(LibraryPathSwitch)/opt/arm-none-eabi/lib/gcc/arm-none-eabi/8.3.1/arm 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := "/opt/arm-none-eabi/bin/arm-none-eabi-ar" rcu
CXX      := "/opt/arm-none-eabi/bin/arm-none-eabi-g++"
CC       := "/opt/arm-none-eabi/bin/arm-none-eabi-gcc"
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -Wmain -g -O0 -std=c99 -Wall -mcpu=cortex-m0plus -mthumb -ffunction-sections -fdata-sections -mno-sched-prolog -gdwarf-2 -fmessage-length=0 -fno-builtin -MMD -MP $(Preprocessors)
ASFLAGS  := 
AS       := "/opt/arm-none-eabi/bin/arm-none-eabi-as"


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(ObjectSuffix) $(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(ObjectSuffix) $(IntermediateDirectory)/src_gfx.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(ObjectSuffix) \
	$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(ObjectSuffix) 

Objects1=$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(ObjectSuffix) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(ObjectSuffix) 



Objects=$(Objects0) $(Objects1) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	@echo $(Objects1) >> $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	/opt/arm-none-eabi/bin/arm-none-eabi-size ./Debug/OLED_TESTING_1.0
	/opt/arm-none-eabi/bin/arm-none-eabi-objcopy -O binary ./Debug/OLED_TESTING_1.0 ./Debug/OLED_TESTING_1.0.bin
	@echo Done

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(ObjectSuffix): src/ASF/sam0/utils/cmsis/samda1/source/gcc/startup_samda1.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(DependSuffix) -MM src/ASF/sam0/utils/cmsis/samda1/source/gcc/startup_samda1.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/utils/cmsis/samda1/source/gcc/startup_samda1.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(PreprocessSuffix): src/ASF/sam0/utils/cmsis/samda1/source/gcc/startup_samda1.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_gcc_startup_samda1.c$(PreprocessSuffix) src/ASF/sam0/utils/cmsis/samda1/source/gcc/startup_samda1.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(ObjectSuffix): src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/i2c_master.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(DependSuffix) -MM src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/i2c_master.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/i2c_master.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(PreprocessSuffix): src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/i2c_master.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_i2c_i2c_sam0_i2c_master.c$(PreprocessSuffix) src/ASF/sam0/drivers/sercom/i2c/i2c_sam0/i2c_master.c

$(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(ObjectSuffix): src/ASF/sam0/boards/samda1_xplained_pro/board_init.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(DependSuffix) -MM src/ASF/sam0/boards/samda1_xplained_pro/board_init.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/boards/samda1_xplained_pro/board_init.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(PreprocessSuffix): src/ASF/sam0/boards/samda1_xplained_pro/board_init.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_boards_samda1_xplained_pro_board_init.c$(PreprocessSuffix) src/ASF/sam0/boards/samda1_xplained_pro/board_init.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(ObjectSuffix): src/ASF/sam0/drivers/system/pinmux/pinmux.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(DependSuffix) -MM src/ASF/sam0/drivers/system/pinmux/pinmux.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/system/pinmux/pinmux.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(PreprocessSuffix): src/ASF/sam0/drivers/system/pinmux/pinmux.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_pinmux_pinmux.c$(PreprocessSuffix) src/ASF/sam0/drivers/system/pinmux/pinmux.c

$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(ObjectSuffix): src/ASF/sam0/utils/cmsis/samda1/source/system_samda1.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(DependSuffix) -MM src/ASF/sam0/utils/cmsis/samda1/source/system_samda1.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/utils/cmsis/samda1/source/system_samda1.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(PreprocessSuffix): src/ASF/sam0/utils/cmsis/samda1/source/system_samda1.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_utils_cmsis_samda1_source_system_samda1.c$(PreprocessSuffix) src/ASF/sam0/utils/cmsis/samda1/source/system_samda1.c

$(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(ObjectSuffix): src/ASF/common/utils/interrupt/interrupt_sam_nvic.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(DependSuffix) -MM src/ASF/common/utils/interrupt/interrupt_sam_nvic.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/common/utils/interrupt/interrupt_sam_nvic.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(PreprocessSuffix): src/ASF/common/utils/interrupt/interrupt_sam_nvic.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_common_utils_interrupt_interrupt_sam_nvic.c$(PreprocessSuffix) src/ASF/common/utils/interrupt/interrupt_sam_nvic.c

$(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(ObjectSuffix): src/qs_i2c_master_basic_use.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(DependSuffix) -MM src/qs_i2c_master_basic_use.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/qs_i2c_master_basic_use.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(PreprocessSuffix): src/qs_i2c_master_basic_use.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_qs_i2c_master_basic_use.c$(PreprocessSuffix) src/qs_i2c_master_basic_use.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(ObjectSuffix): src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/gclk.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(DependSuffix) -MM src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/gclk.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/gclk.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(PreprocessSuffix): src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/gclk.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_gclk.c$(PreprocessSuffix) src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/gclk.c

$(IntermediateDirectory)/src_gfx.c$(ObjectSuffix): src/gfx.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_gfx.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_gfx.c$(DependSuffix) -MM src/gfx.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/gfx.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_gfx.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_gfx.c$(PreprocessSuffix): src/gfx.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_gfx.c$(PreprocessSuffix) src/gfx.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(ObjectSuffix): src/ASF/sam0/drivers/sercom/sercom.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(DependSuffix) -MM src/ASF/sam0/drivers/sercom/sercom.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/sercom/sercom.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(PreprocessSuffix): src/ASF/sam0/drivers/sercom/sercom.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_sercom_sercom.c$(PreprocessSuffix) src/ASF/sam0/drivers/sercom/sercom.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(ObjectSuffix): src/ASF/sam0/drivers/port/port.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(DependSuffix) -MM src/ASF/sam0/drivers/port/port.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/port/port.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(PreprocessSuffix): src/ASF/sam0/drivers/port/port.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_port_port.c$(PreprocessSuffix) src/ASF/sam0/drivers/port/port.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(ObjectSuffix): src/ASF/sam0/drivers/system/interrupt/system_interrupt.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(DependSuffix) -MM src/ASF/sam0/drivers/system/interrupt/system_interrupt.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/system/interrupt/system_interrupt.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(PreprocessSuffix): src/ASF/sam0/drivers/system/interrupt/system_interrupt.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_interrupt_system_interrupt.c$(PreprocessSuffix) src/ASF/sam0/drivers/system/interrupt/system_interrupt.c

$(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(ObjectSuffix): src/ASF/sam0/utils/syscalls/gcc/syscalls.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(DependSuffix) -MM src/ASF/sam0/utils/syscalls/gcc/syscalls.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/utils/syscalls/gcc/syscalls.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(PreprocessSuffix): src/ASF/sam0/utils/syscalls/gcc/syscalls.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_utils_syscalls_gcc_syscalls.c$(PreprocessSuffix) src/ASF/sam0/utils/syscalls/gcc/syscalls.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(ObjectSuffix): src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/clock.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(DependSuffix) -MM src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/clock.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/clock.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(PreprocessSuffix): src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/clock.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_clock_clock_samd21_r21_da_clock.c$(PreprocessSuffix) src/ASF/sam0/drivers/system/clock/clock_samd21_r21_da/clock.c

$(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(ObjectSuffix): src/ASF/sam0/drivers/system/system.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(ObjectSuffix) -MF$(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(DependSuffix) -MM src/ASF/sam0/drivers/system/system.c
	$(CC) $(SourceSwitch) "/home/william/projects/oled_testing/OLED_TESTING_1.0/src/ASF/sam0/drivers/system/system.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(PreprocessSuffix): src/ASF/sam0/drivers/system/system.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_ASF_sam0_drivers_system_system.c$(PreprocessSuffix) src/ASF/sam0/drivers/system/system.c


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


