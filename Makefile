.PHONY: clean All

All:
	@echo "----------Building project:[ OLED_TESTING_1.0 - Debug ]----------"
	@"$(MAKE)" -f  "OLED_TESTING_1.0.mk" && "$(MAKE)" -f  "OLED_TESTING_1.0.mk" PostBuild
clean:
	@echo "----------Cleaning project:[ OLED_TESTING_1.0 - Debug ]----------"
	@"$(MAKE)" -f  "OLED_TESTING_1.0.mk" clean
