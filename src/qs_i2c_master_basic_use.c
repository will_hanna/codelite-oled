#include <asf.h>
#include <gfx.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                         //
//                                MAIN FUNCTION BEGINS - OLED DISPLAY TESTIN                               //
//                                                                                                         //
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
int main(void)
{
	system_init();
        configure_i2c_master();
        OLED_setup();
        OLED_CLR();
        OLED_TIC_UPDATE();
        OLED_UPDATE();
        OLED_FONT_SETUP();    
        //OLED_hex_print(10,4, 8, 1);
        //OLED_line(0, 0, 20,20,1);
        //OLED_line(20, 20, 40,0,1);
        //OLED_line(0, 40, 20,20,1);
        //OLED_line(20, 20, 40,40,1);
        //OLED_CIRCLE(10, 50, 8, 1);
        //OLED_CIRCLE(80, 25, 25, 1);
        pass_string("!@#$%^&*()(",0,0,1);
        pass_string("abcABCzZ123",0,8,1);
	while(1){
        OLED_TIC_UPDATE();
        //OLED_CLR();
        //OLED_UPDATE();
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
//                                  OLED FUNCTIONS                                                   //
//                                                                                                   //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//

//! [initialize_i2c]
/*void configure_i2c_master(void)
{
	struct i2c_master_config config_i2c_master;
	i2c_master_get_config_defaults(&config_i2c_master);
	// Change buffer timeout to something longer.
	//! [conf_change]
	config_i2c_master.buffer_timeout = 1000;
	#if SAMR30
	config_i2c_master.pinmux_pad0    = CONF_MASTER_SDA_PINMUX;
	config_i2c_master.pinmux_pad1    = CONF_MASTER_SCK_PINMUX;
	#endif
	//! [conf_change]
	// Initialize and enable device with config.
	//! [init_module]
	i2c_master_init(&i2c_master_instance, CONF_I2C_MASTER_MODULE, &config_i2c_master);
	//! [init_module]

	//! [enable_module]
	i2c_master_enable(&i2c_master_instance);
	//! [enable_module]
     * 
}
//! [initialize_i2c]
//
void OLED_setup()
{
#define DATA_LENGTH_OLED_SETUP 27

static uint8_t OLED_setup[DATA_LENGTH_OLED_SETUP] = {
0x00,0xAE,0x02,0x10,0x40,0xB0,0x81,0x80,0xA1,0xA6,0xA8,0x3F,0xAD,0x8B,0x30,0xC8,0xD3,0x00,0xD5,0x80,0xD9,0x1F,0xDA,0x12,0xDB,0x40,0xAF,};
/* WriteCommand(0xAE);    display off

 WriteCommand(0x02);    set lower column address
 WriteCommand(0x10);    /*set higher column address
WriteCommand(0x40);    /*set display start line
 WriteCommand(0xB0);    /*set page address
 WriteCommand(0x81);    /*contract control
 WriteCommand(0x80);    /*128
 WriteCommand(0xA1);    /*set segment remap
 WriteCommand(0xA6);    /*normal / reverse
 WriteCommand(0xA8);    /*multiplex ratio
 WriteCommand(0x3F);    /*duty = 1/32
 WriteCommand(0xad);    /*set charge pump enable
 WriteCommand(0x8b);     /*external VCC   
 WriteCommand(0x30);    /*0X30---0X33  set VPP   9V liangdu!!!!
 WriteCommand(0xC8);    /*Com scan direction
 WriteCommand(0xD3);    /*set display offset
 WriteCommand(0x00);   /*   0x20  
 WriteCommand(0xD5);    /*set osc division
 WriteCommand(0x80);
 WriteCommand(0xD9);    /*set pre-charge period
 WriteCommand(0x1f);    /*0x22
 WriteCommand(0xDA);    /*set COM pins
 WriteCommand(0x12);
 WriteCommand(0xdb);    /*set vcomh
 WriteCommand(0x40);
 WriteCommand(0xAF); /*display ON*/
// 00 - command string
// AE - display OFF
// A8 - MUX Ratio			= 0x3F
// D3 - Display offset		= 0x00
// 04 - Start Line			= 0xA1
// C8 - COM Scan Modde
// DA - COM Pin map			= 0x12
// 81 - Contrast			= 0x7F
// A4 - Display ram
// A6 - Display normal
// D5 - CLK Div				= 0x80
// 8D - Charge Pump         = 0x14
// D9 - Precharge			= 0x22
// DB - VCOM_H Desel		= 0x30
// 20 - Memory address mode = 0x00
// 00 - set col
// AF - Display ON
/*
OLED_data_packet(OLED_setup,DATA_LENGTH_OLED_SETUP);
}

void OLED_data_packet(int packet[], int size)
{
struct i2c_master_packet data_packet = {
	.address     = SLAVE_ADDRESS,
	.data_length = size,
	.data        = packet,
	.ten_bit_address = false,
	.high_speed      = true,
	.hs_master_code  = 0x0F,
};
uint16_t timeout = 0;
// Write buffer to slave until success.
while (i2c_master_write_packet_wait(&i2c_master_instance, &data_packet) !=
STATUS_OK) {
	// Increment timeout counter and check if timed out.
	if (timeout++ == TIMEOUT) {
		break;
	}
}
}

void OLED_CLR(void)
{
	int count=0;
	uint8_t clr_buffer[129]={ 0 };
		for (count=0;count<130;count++)
		{
			clr_buffer[count]=0x00
			;
		}
	uint8_t comm_buffer[4]={0};
	uint8_t page=0;	
	clr_buffer[0]=0x40;
		for (page=0;page<8;page++)
		{
			comm_buffer[0]=0x00;
			comm_buffer[1]=0x02; //column lower bit
			comm_buffer[2]=0x10; //column upper bit
			comm_buffer[3]=0xB0+page;//0+page;
		OLED_data_packet(comm_buffer,4);
		OLED_data_packet(clr_buffer,129);
		}
};
*/
